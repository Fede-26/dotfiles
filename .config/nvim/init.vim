call plug#begin('~/.local/share/nvim/plugged')

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'zchee/deoplete-jedi'

" status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" auto closing brackets
"Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'

" commenter
"Plug 'scrooloose/nerdcommenter'

" formatter
"Plug 'sbdchd/neoformat'

" code jump (go-to)
"Plug 'davidhalter/jedi-vim'

" sidebar
Plug 'scrooloose/nerdtree'

" linter
"Plug 'neomake/neomake'

" highlight copied sections
Plug 'machakann/vim-highlightedyank'

" code folding
"Plug 'tmhedberg/SimpylFold'

" theme: gruvbox
"Plug 'morhetz/gruvbox'

" theme: base-16
"Plug 'chriskempson/base16-vim'

" tabular plugin is used to format tables
"Plug 'godlygeek/tabular'

" JSON front matter highlight plugin
"Plug 'elzr/vim-json'

" Syntax markdown
"Plug 'plasticboy/vim-markdown'
Plug 'vim-pandoc/vim-pandoc-syntax'

" Markdown preview
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }

" Code navigator
Plug 'easymotion/vim-easymotion'

" Fuzzy finder
Plug 'ctrlpvim/ctrlp.vim'

" Emmet for HTML
Plug 'mattn/emmet-vim'

" Interactive REPL
Plug 'metakirby5/codi.vim'

call plug#end()

" ========[PERSONALI]========

set number relativenumber

set mouse=a

syntax enable

set ts=4

set autoindent

set expandtab

set shiftwidth=4

set cursorline

set showmatch

let mapleader = ","

map <C-\> :NERDTreeToggle<CR>

noremap E $
" ===========================


let g:deoplete#enable_at_startup = 1

" autodisable the suggestion window
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" use tab to go trough autocompletitons
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"


" airline theme
let g:airline_theme='bubblegum'
let g:airline_powerline_fonts = 1


" disable autocompletion, cause we use deoplete for completion
let g:jedi#completions_enabled = 0

" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"


" select pylint for python
"let g:neomake_python_enabled_makers = ['pylint']

" auto error checker
"call neomake#configure#automake('nrwi', 500)


" gruvbox
set background=dark " or light

" base-16
"let base16colorspace = 256
"colorscheme base16-default-dark


" MARKDOWN ↓↓

" disable header folding
let g:vim_markdown_folding_disabled = 1

" do not use conceal feature, the implementation is not so good
let g:vim_markdown_conceal = 0

" disable math tex conceal feature
let g:tex_conceal = ""
let g:vim_markdown_math = 1

" support front matter of various format
let g:vim_markdown_frontmatter = 1  " for YAML format
let g:vim_markdown_toml_frontmatter = 1  " for TOML format
let g:vim_markdown_json_frontmatter = 1  " for JSON format

augroup pandoc_syntax
    au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
augroup END

" markdown preview
" do not close the preview tab when switching to other buffers
let g:mkdp_auto_close = 0
let g:mkdp_browser = 'firefox'
nmap <C-s> <Plug>MarkdownPreview


" <Leader>S easy motion
map <Leader> <Plug>(easymotion-prefix)
