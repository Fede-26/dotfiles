set PATH /home/federico/.local/bin/ $PATH
set PAGER "less"

#set BASE16_SHELL "$HOME/.config/base16-shell/"
#source "$BASE16_SHELL/profile_helper.fish"

# Base16 Shell
if status --is-interactive
#    fish_vi_key_bindings
    fish_hybrid_key_bindings

    alias pro="cd /run/media/federico/large-file_part/"
    alias musica="cd /media/federico/large-file_part/musica/all/"
#    alias nvim="/home/federico/software/nvim.appimage"
    alias ran="ranger"
    alias ytmus="youtube-dl -x --audio-format flac"
    alias i3conf="nvim /home/federico/.config/i3/config"
    alias fishconf="nvim /home/federico/.config/fish/config.fish"
    alias conda="/home/federico/anaconda3/bin/conda"
    alias config='/usr/bin/git --git-dir=/home/federico/.cfg/ --work-tree=/home/federico'
    alias md2pdf.sh='~/scripts/md2pdf.sh'
    alias aseprite="/home/federico/software/aseprite/build/bin/aseprite"
    alias cp="cp -iv"
    set TUIR_EDITOR "vim"
end

