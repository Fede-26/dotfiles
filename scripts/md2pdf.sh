#!/usr/bin/bash

mdfile=$1
param=$2
filename=${mdfile%".md"}
#pdffile=$filename"_"$(date +"%F")".pdf"
pdffile=$filename".pdf"
extensions="+escaped_line_breaks-blank_before_header+space_in_atx_header+fancy_lists+fenced_divs+definition_lists+tex_math_dollars"

echo "engines: context pdflatex weasyprint xelatex lualatex prince wkhtmltopdf"
echo $mdfile
echo $filename
echo $pdffile

pandoc $mdfile \
    -f markdown$extensions \
    --template /home/federico/.pandoc/templates/eisvogel.latex \
    -V lang=it \
    --listings \
    --pdf-engine=xelatex \
    --katex \
    --standalone \
    -o $pdffile \
    $param

#   --number-sections \
#   -V geometry:a4paper \
#   -V geometry:margin=2cm \
#   -V mainfont="DejaVu Serif" \
#   -V monofont="DejaVu Sans Mono" \
#   -V fontsize=12pt \
#   --mathjax \
#   --pdf-engine $engine \
