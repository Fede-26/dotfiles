#!/usr/bin/bash

audioName1="record-n-play"
audioDesc1="Record-and-Play"
audioName2="meet_card"
audioDesc2="Meet-audio-card"
videoName1="Webcam-bob"
videoNum1=8
videoName2="Webcam-jimmy"
videoNum2=9



DEFAULT_OUTPUT=$(pacmd list-sinks | grep -A1 "* index" | grep -oP "<\K[^ >]+")

pactl load-module module-combine-sink \
  sink_name=$audioName1 slaves=$DEFAULT_OUTPUT \
  sink_properties=device.description=$audioDesc1

pactl load-module module-combine-sink \
  sink_name=$audioName2 slaves=$DEFAULT_OUTPUT \
  sink_properties=device.description=$audioDesc2

sudo modprobe v4l2loopback video_nr=$videoNum1,$videoNum2 card_label=$videoName1,$videoName2


echo "audioName1="$audioName1
echo "audioDesc1="$audioDesc1
echo "audioName2="$audioName2
echo "audioDesc2="$audioDesc2
echo "videoName1="$videoName1
echo "videoNum1="$videoNum1
echo "videoName2="$videoName2
echo "videoNum2="$videoNum2
