#!/usr/bin/env bash

sudo echo ""
echo -e "\n\n--APT UPDATE--\n"
sudo apt update
echo -e "\n\n--APT UPGRADE--\n"
sudo apt upgrade

echo -e "\n\n--FLATPAK--\n"
flatpak update

echo -e "\n\n--SNAP--\n"
sudo snap refresh


echo -e "\n\n--APT AUTOREMOVE--\n"
sudo apt autoremove
echo -e "\n\n--APT AUTOCLEAN--\n"
sudo apt autoclean
