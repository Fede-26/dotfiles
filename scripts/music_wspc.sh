#!/usr/bin/bash

#i3-msg focus output right
#i3-msg workspace 10
#i3-msg rename workspace to "10:Music"
#i3-msg focus output primary
#i3-msg workspace 1

i3-msg rename workspace to "Music"
i3-msg layout splitv

i3-msg append_layout /home/federico/.layouts/music.json

gnome-terminal > /dev/null 2>&1 & 
gnome-terminal > /dev/null 2>&1 & 
cantata > /dev/null 2>&1 & 
telegram-desktop > /dev/null 2>&1 & 

i3-msg move workspace to output right

i3-msg focus workspace 1
