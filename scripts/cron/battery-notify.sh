#!/usr/bin/bash

export DISPLAY=$(who -u | awk  '/\s:[0-9]\s/ {print $2}')
XDG_RUNTIME_DIR=/run/user/$(id -u)
#XAUTHORITY=/home/federico/.Xauthority

bat=$(cat /sys/class/power_supply/BAT0/capacity)

echo $(date +"%F %T") $bat"%"

if [ $bat -lt 30 ] ; then
    grep "Charging" "/sys/class/power_supply/BAT0/status" || notify-send "Battery remaining: $bat %"
fi

#if [ $bat -gt 30 ] ; then
#    notify-send "Battery ok: $bat %"
#fi
